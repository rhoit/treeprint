/*
 Title: Configuration for OS
 Started: Oct 2008
 Last Update: 9 Oct 2008
*/
#ifdef MSWin
#include<conio.h>
#define cls system("cls")
#else
#define cls system("clear")
#include <stdlib.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
int getch()
{
  struct termios oldt, newt;
  int ch;
  tcgetattr(STDIN_FILENO, &oldt);
  newt= oldt;
  newt.c_lflag &= ~(ICANON| ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  ch=getchar();
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  return ch;
}
#endif
